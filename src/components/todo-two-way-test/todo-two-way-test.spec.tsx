import { TestWindow } from '@stencil/core/testing';
import { TodoTwoWayTest } from './todo-two-way-test';

describe('todo-two-way-test', () => {
  it('should build', () => {
    expect(new TodoTwoWayTest()).toBeTruthy();
  });

  describe('rendering', () => {
    let element;
    let window;

    beforeEach(async () => {
      window = new TestWindow();
      element = await window.load({
        components: [TodoTwoWayTest],
        html: '<todo-two-way-test></todo-two-way-test>'
      });
    });

    it('should work with both the first and the last name', async () => {
      element.first = 'Peter';
      element.last = 'Parker';
      await window.flush();
      expect(element.textContent).toEqual('Hello, my name is Peter Parker');
    });
  });
});
