import {Component, Prop} from '@stencil/core';

@Component({
  tag: 'todo-two-way-test',
  styleUrl: 'todo-two-way-test.css',
  shadow: true
})
export class TodoTwoWayTest {
  @Prop({ mutable: true }) coolNumber: number;
  textInput: HTMLInputElement;

  updateNumber(event) {
      this.coolNumber = event.target.value;
  }

  componentDidLoad() {
    this.textInput.value = this.coolNumber.toString();
  }

  render() {
    return (
      <div>
        <h4>Ugly in component: </h4>
        <h5>{this.coolNumber}</h5>
        <input type="number" ref={(el: HTMLInputElement) => this.textInput = el} onChange={this.updateNumber.bind(this)}/>
      </div>
    );
  }
}
